# Poetician-Fonts
Some of these fonts have been on my system for up to a decade,
and any attribution files are somewhere on the internet, but not necessarily here.
They're all freely available for private use regardless, or I wouldn't use them.
Recently downloaded fonts are/might be in both zipped and extracted format.
